*** Settings ***
Documentation     Проверяет работу библиотеки на Java.
...
...               Для проверки написана тестовая библиотека с ключевыми
...               словами TestKeywordLibrary.

Metadata          Version   1.0.0-SNAPSHOT

# http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#setting-custom-name-to-test-library
Library           com.tohasan.TestKeywordLibrary    WITH NAME   TestKeywordLibrary

*** Test Cases ***

Проверка
    createAnEmptyStack